# Learn React Authentication

## Starter

```bash
npm create vite@latest project-name -- --template react

# prettier
npm install --save-dev --save-exact prettier

# redux
npm install @reduxjs/toolkit react-redux

# router
npm i react-router-dom
```

## Sources

### I. Lesson 01

React Redux Login Authentication Flow with JWT Access, Refresh Tokens, Cookies with project name: [redux-login-authentication](https://www.youtube.com/watch?v=-JJFQ9bkUbo&list=PL0Zuz27SZ-6M1J5I1w2-uZx36Qp6qhjKo&index=9)
